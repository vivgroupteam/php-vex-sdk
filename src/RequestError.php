<?php
namespace VivDesign\PhpVexSdk;
use Exception;

class RequestError extends Exception {
    private $_data = '';

    public function __construct($message, $data) 
    {
        $this->_data = $data;
        $message = $message . '! ' . $this->prepareMessageData($data);
        parent::__construct($message);
    }

    public function getData()
    {
        return $this->_data;
    }

    private function prepareMessageData ($message) {
        $result = '';
        if (\is_array($message) || \is_object($message)) {
            foreach ($message as $msg) {
                if (\is_array($msg)) {
                    $msg = $msg[0];
                }
                $result .= '' . $msg . ' ';
             }
        } else {
            $result = $message;
        }

        return $result;
    }
}