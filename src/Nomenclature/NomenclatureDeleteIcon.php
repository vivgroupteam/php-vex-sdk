<?php
namespace VivDesign\PhpVexSdk\Nomenclature;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * View parameters class
 * @var $id integer
 */

class NomenclatureDeleteIcon extends Params {
    /**
     * 
     * @var $id integer
     */
    public int $id;


    /**
     * Nomenclature delete icon constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'nomenclature.nomenclature.deleteIcon', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}