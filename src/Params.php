<?php

namespace VivDesign\PhpVexSdk;

use Adbar\Dot;
use Exception;
use ReflectionProperty;

abstract class Params
{
    /**
     * Request response
     *
     * @var object $response
     */
    private object $response;

    /**
     * Validate parameters types
     *
     * @param array $params
     * @return object
     */
    public function validateParams(array $params): object
    {
        // Check properies types
        foreach ($params as $key => $param) {
            $rp = new ReflectionProperty($this, $key);
            $propType = $rp->getType()->getName();
    
            $func = 'is_' . $propType;
            if (!$func($param)) {
                throw new Exception($key . ' parameter should be of type '
                    . $propType . ' - ' . getType($param) .  ' presented.');
            }
        }

        return $this;
    }

    /**
     * Set parameters to object properties
     *
     * @param array $params
     * @return object
     */
    public function setParams(array $params): object
    {
        // Set params to object properties
        foreach ($params as $key => $param) {
            if (!empty($schema) && isset($schema[$key])) {
                $this->$schema[$key] = $param;
            } else {
                $this->$key = $param;
            }
        }

        return $this;
    }

    public function prepareParamsBySchema(array $params, array $schema)
    {
        $dot = new Dot();
        //var_dump($dot->pull('user.test'));die;
        // var_dump( $dot->set('user.name'));die;
        foreach ($schema as $sourceKey => $destKey) {
            $newParams[$destKey] = $params[$sourceKey];
        }
        return $newParams;
    }

    /**
     * Set response function
     *
     * @param object $response
     * @return void
     */
    public function setResponse(object $response)
    {
        $this->response = $response;
    }

    /**
     * Class properties to array
     *
     * @return array
     */
    public function toArray(): array
    {
        return (array) $this;
    }

    /**
     * Get response propery
     *
     * @return object
     */
    public function response(): object
    {
        return $this->response;
    }
}
