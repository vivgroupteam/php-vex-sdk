<?php
namespace VivDesign\PhpVexSdk\Product;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * View parameters class
 * @var $id integer
 * @var $slug string
 * @var $with array
 * @var $groupNmclByCode bool
 * @var $breadcrumbs bool
 */

class ProductView extends Params {
    /**
     * 
     * @var $id integer
     */
    public int $id;

    /**
     * 
     * @var $slug string
     */
    public string $slug;

    /**
     * 
     * @var $with array
     */
    public array $with;

    /**
     * 
     * @var $groupNmclByCode bool
     */
    public bool $groupNmclByCode;

    /**
     * 
     * @var $breadcrumbs bool
     */
    public bool $breadcrumbs;

    /**
     * Product view constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'product.product.view', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}