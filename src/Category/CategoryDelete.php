<?php
namespace VivDesign\PhpVexSdk\Category;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

class CategoryDelete extends Params {
    /*
        'id' => 'reuired|integer'
    */

    /**
    * 
    * @var $id int
    */
    public int $id;

    /**
     * Category view constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'category.category.delete', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}