<?php
namespace VivDesign\PhpVexSdk\Category;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

class CategoryStore extends Params {
    /*
        'lock' => 'filled|boolean',
        'status' => 'filled|boolean',
        'parent_id' => 'nullable|alpha_num',
        'prepend' => 'filled|boolean',
        'before' => 'filled|integer',
        'after' => 'filled|integer',
        'translations.%title%' => 'required|string',
        'translations.%description%' => 'filled|string',
        'translations.%meta_title%' => 'filled|string',
        'translations.%meta_description%' => 'filled|string',
        'permissions' => 'sometimes|array',
        'permissions.*.permission_id' => 'required_with:permissions|integer',
        'permissions.*.user_relation' => 'required_with:permissions|string',
        'permissions.*.user_relation_id' => 'required_with:permissions|array'
    */

    /**
    * 
    * @var $lock bool
    */
    public bool $lock;

    /**
     * 
     * @var $status bool
     */
    public bool $status;

    /**
     * 
     * @var $parent_id int
     */
    public int $parent_id;

    /**
     * 
     * @var $code string
     */
    public string $code;

    /**
     * @var array $translations[Locale]
     */
    public array $translations = [
        // 'en' => [
        //     'title' => '',
        //     'description' => '',
        //     'meta_title' => '',
        //     'meta_description' => '',
        // ]
    ];

    /**
     * Category view constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'category.category.store', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}