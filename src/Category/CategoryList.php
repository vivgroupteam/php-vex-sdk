<?php
namespace VivDesign\PhpVexSdk\Category;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * View parameters class
 * @var $type string
 * @var $with array
 * @var $mutations array
 */

class CategoryList extends Params {
    /**
     * 
     * @var $type string
     */
    public string $type;

    /**
     * 
     * @var $with array
     */
    public array $with;

    /**
     * 
     * @var $mutations array
     */
    public array $mutations;


    /**
     * Category list constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'category.category.list', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}