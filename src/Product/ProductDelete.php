<?php
namespace VivDesign\PhpVexSdk\Product;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * View parameters class
 * @var $id integer
 */

class ProductDelete extends Params {
    /**
     * 
     * @var $id integer
     */
    public int $id;


    /**
     * Product delete constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'product.product.delete', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}