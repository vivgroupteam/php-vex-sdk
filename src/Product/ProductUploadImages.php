<?php
namespace VivDesign\PhpVexSdk\Product;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Upload images parameters class
 * @var $id int
 * @var $file array
 * [
 *      [
 *          'filename' => 'filename.jpg',
 *          'content' => 'base64 encoded image'
 *      ]
 * ]
 * 
 */

class ProductUploadImages extends Params {
    /**
     * 
     * @var $id int
     */
    public int $id;

    /**
     * 
     * @var $file array
     */
    public array $file;

    /**
     * Product upload images constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'product.product.uploadImages', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}