<?php
namespace VivDesign\PhpVexSdk\Inventory\Inventory;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Save parameters class
 * @var $product_id int
 * @var $warehouse_id int
 * @var $quantity int
 */

class InventorySave extends Params {
    /**
     * 
     * @var $product_id int
     */
    public int $product_id;

    /**
     * 
     * @var $warehouse_id int
     */
    public int $warehouse_id;

    /**
     * 
     * @var $quantity int
     */
    public int $quantity;

    /**
     * Inventory save constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'inventory.inventory.save', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}