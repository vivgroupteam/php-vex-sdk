<?php
namespace VivDesign\PhpVexSdk\Nomenclature;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Get available nomenclature types class
 */

class NomenclatureAvailableTypes extends Params {
    /**
     * Nomenclature available types constructor
     *
     * @param Credentials $credentials
     */

    public function __construct (
        Credentials $credentials
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // Make request
        $response = $connection->request(
            'nomenclature.nomenclature.getAvailableTypes', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}