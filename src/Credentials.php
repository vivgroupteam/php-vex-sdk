<?php
namespace VivDesign\PhpVexSdk;

use ErrorException;

class Credentials {
    public $token;
    public $jwt_token;
    public $url;

    /**
     * Credentials
     *
     * @param array{
     *  token: string,
     *  url: string,
     *  jwt_token: string
     * } $credentials
     *  
     */
    public function __construct(array $credentials)
    {
        if (!isset($credentials['token'])) {
            throw new ErrorException('Bearear token missing');
        }

        if (!isset($credentials['url'])) {
            throw new ErrorException('Url missing');
        }

        $this->token = $credentials['token'];
        $this->url = $credentials['url'];

        if (isset($credentials['jwt_token'])) {
            $this->jwt_token = $credentials['jwt_token'];
        }
    }
}