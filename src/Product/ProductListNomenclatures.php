<?php
namespace VivDesign\PhpVexSdk\Product;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Product nomenclatures parameters class
 * @var $productId int
 * @var $categories array
 */

class ProductListNomenclatures extends Params {
    /**
     * 
     * @var $productId int
     */
    public int $productId;

    /**
     * 
     * @var $categories array
     */
    public array $categories;


    /**
     * Product list synced nomenclatures constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'product.product.nomenclatures', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}