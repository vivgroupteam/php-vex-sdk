<?php
namespace VivDesign\PhpVexSdk\Product;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Delete prices parameters class
 * @var $prices array
 * [
 *        'pricelist_id' => 1,
 *        'product_id' => 1234
 * ]
 */

class PricelistDeletePrices extends Params {
    /**
     * 
     * @var $prices array
     */
    public array $prices;


    /**
     * Product delete multiple prices constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'product.pricelist.deletePrices', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}