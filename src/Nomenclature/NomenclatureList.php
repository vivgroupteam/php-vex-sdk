<?php
namespace VivDesign\PhpVexSdk\Nomenclature;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Nomenclatures list parameters class
 * @var $paginate bool
 * @var $with array
 * @var $mutations array
 */

class NomenclatureList extends Params {
    /**
     * 
     * @var $paginate bool
     */
    public bool $paginate;

    /**
     * 
     * @var $with array
     */
    public array $with;

    /**
     * 
     * @var $mutations array
     */
    public array $mutations;


    /**
     * Nomenclature list constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'nomenclature.nomenclature.list', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}