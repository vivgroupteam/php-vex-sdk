<?php
namespace VivDesign\PhpVexSdk\Product;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Update parameters class
 * @var $id int
 * @var $deleteMissingPrices bool
 * @var $code string
 * @var $type string
 * @var $base_pricegroup int
 * @var $priority int
 * @var $currency_id int
 * @var $status bool
 * @var $starts_at string
 * @var $ends_at string
 * @var $prices array
 * [
 *        'value' => 'TV Samsung',
 *        'product_id' => 'Product description'
 * ]
 */

class PricelistUpdate extends Params {
    /**
     * 
     * @var $id int
     */
    public int $id;

    /**
     * 
     * @var $code string
     */
    public string $code;

    /**
     * 
     * @var $type string
     */
    public string $type;

    /**
     * 
     * @var $priority int
     */
    public int $priority;

    /**
     * 
     * @var $currency_id int
     */
    public int $currency_id;

    /**
     * 
     * @var $base_pricegroup int
     */
    public int $base_pricegroup;

    /**
     * 
     * @var $status bool
     */
    public bool $status;

    /**
     * 
     * @var $deleteMissingPrices bool
     */
    public bool $deleteMissingPrices;

    /**
     * 
     * @var $starts_at string
     */
    public string $starts_at;

    /**
     * 
     * @var $ends_at string
     */
    public string $ends_at;

    /**
     * 
     * @var $prices array
     */
    public array $prices;


    /**
     * Product update pricelist constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'product.pricelist.update', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}