<?php
namespace VivDesign\PhpVexSdk\Inventory\Warehouse;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * List parameters class
 * @var $with array
 * @var $mutations array
 * @var $paginate bool
 */

class WarehouseList extends Params {
    /**
     * 
     * @var $with array
     */
    public array $with;

    /**
     * 
     * @var $mutations array
     */
    public array $mutations;

    /**
     * 
     * @var $paginate bool
     */
    public bool $paginate;

    /**
     * Warerhouse list constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'inventory.warehouse.list', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}