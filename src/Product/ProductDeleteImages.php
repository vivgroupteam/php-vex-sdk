<?php
namespace VivDesign\PhpVexSdk\Product;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Delete images parameters class
 * @var $id array
 */

class ProductDeleteImages extends Params {
    /**
     * 
     * @var $id array
     */
    public array $id;

    /**
     * Product delete images constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'product.product.deleteImages', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}