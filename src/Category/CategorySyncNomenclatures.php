<?php
namespace VivDesign\PhpVexSdk\Category;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * View parameters class
 * @var $id int
 * @var $nomenclatures array
 */

class CategorySyncNomenclatures extends Params {
    /**
     * 
     * @var $id int
     */
    public int $id;

    /**
     * 
     * @var $nomenclatures array
     */
    public array $nomenclatures;


    /**
     * Category sync nomenclatures constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'category.category.nomeclatureSync', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}