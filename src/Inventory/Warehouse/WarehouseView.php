<?php
namespace VivDesign\PhpVexSdk\Inventory\Warehouse;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * View parameters class
 * @var $id int
 */

class WarehouseView extends Params {
    /**
     * 
     * @var $id int
     */
    public int $id;

    /**
     * Warehouse view constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'inventory.warehouse.view', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}