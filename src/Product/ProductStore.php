<?php
namespace VivDesign\PhpVexSdk\Product;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Store parameters class
 * @var $code string
 * @var $status bool
 * @var $order int
 * @var $translations array
 * [
 *    'en' => [
 *        'name' => 'TV Samsung',
 *        'description' => 'Product description'
 *    ] 
 * ]
 * @var $categories array of IDs
 * @var $location string
 * @var $nmclPairs array 
 * 'color' => [
 *    [
 *       'locale' => 'en',
 *       'text' => 'White'
 *    ]
 * ]
 * @var $prices array
 * [
 *     'base' => 29.90
 *     'promo' => 21.89
 * ]
 * @var $address array
 */

class ProductStore extends Params {
    /**
     * 
     * @var $code string
     */
    public string $code;

    /**
     * 
     * @var $status bool
     */
    public bool $status;

    /**
     * 
     * @var $order int
     */
    public int $order;

    /**
     * 
     * @var $translations array
     */
    public array $translations;

    /**
     * 
     * @var $categories array
     */
    public array $categories;

    /**
     * 
     * @var $location string
     */
    public string $location;

    /**
     * 
     * @var $nmclPairs array
     */
    public array $nmclPairs;

    /**
     * 
     * @var $prices array
     */
    public array $prices;

    /**
     * 
     * @var $address array
     */
    public array $address;

    /**
     * Product store constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'product.product.store', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}