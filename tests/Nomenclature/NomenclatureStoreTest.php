<?php

use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Nomenclature\NomenclatureStore;

test('Nomenclature Store', function () {
    $category = new NomenclatureStore(
        new Credentials([
            'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI2IiwianRpIjoiZmM5YzkyNTkxZWFiMDM5ZjhmMmRkMzMxNGZiOWM5MGQ1MWZmYTY3MjBkMmQwMmMyZDQ0NTFjODFhYjFmNDQzNDFiOTAwMTg1OTAzNjYyMDUiLCJpYXQiOjE3MDY3MDA3MjEuODA2NDcsIm5iZiI6MTcwNjcwMDcyMS44MDY0NzUsImV4cCI6MTczODIzNjcyMS43OTc5NDIsInN1YiI6IiIsInNjb3BlcyI6W119.iiihCF-bEvVqX8bxCA608QrMJ6hGW5Mhmh3IeBTKHR6k-UtJ4hpXj7hCFa16zsyDZt5Q8VaDa5jmqcbDOJHXrZ42WoWsq50uDjxRf4N4FmPwygAyGSobi_dIdAXPgdOE7ejxyGEOSMKxc_C1CJo42veTr89Vl_OT5iTT8lJON2-AMsUI_rbNGH0A0Hvx-KwmwqX0u7bO3NeB9MzLww-OhcPC1QspK6YT7rESiaOi4wDG3-NpN4IoEie9-qtWi3X1TeJnaaKUkVhlco1nrqiSScCWP427LHxOPtUXbbvJWrdUrpKtt-DPvHUvFTpKOrgzKYhFV-kXM9oS8y2B8st_tLAbGDJGekXD2FJRPqRrdd4C9bBjR31NWD-kYUNGhbyELlf6uql_z8XqvMwbei6Ll3grZwZgXeSrqEpFd2V6pRlFGI8g8KluoVhRrnQKoCwH5zrCuTkaR0hiA3Zx78_szaIcEEXpSuz6MrwpPykgDrAKy1FjMGKuCoI9V5omrBnMRwCMgmNZyLE3aIgcCcRxvFmHoryKNAfm-1gHByoCpg5JWY-7x-MRw51lw91cznWfhmVC4QexGvun83ms6ZxJUOqllKTfbNbXXmMB0LZ2mj9rTzXT4fO7OONFURk_17lkRpalfkb2eGOaUQmtq9MmDnNwEgfqPJo9ke-Uqkb7k94',
            'jwt_token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJwYXNrb0BzdGVwc29mdC5iZyIsInVpZCI6NCwidmV4U2Vzc2lvbklkIjoiTm51aXpibkU5MnlBWHZ0Y2xzSDBYVUFjWFNyUXVWOWRnZ0o0bVJHMCIsImV4cCI6MTcxMTg3Nzc2NC40NTI1Mzd9.WWUOnNUwRj8TGMszIRPCHT20acAqvoCCErFgf-F5KsI',
            'url' => 'techmartcy-srv.dev.vivgroup.net'
        ]),
        [
            "status" => true,
            "code" => "icon-test-3",
            //"parent" => null,
            "translations" => [
                'en' => [
                    'name' => "con test 3"
                ]
            ]
        ]
    );
    expect($category->response())->toBeObject();
});
