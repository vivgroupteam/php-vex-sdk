<?php
namespace VivDesign\PhpVexSdk;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;

class Connection {
    private $credentials;
    private static $instance = null;

    /**
     * Undocumented function
     *
     * @param Credentials $credentials
     */
    public function __construct(Credentials $credentials)
    {
        $this->credentials =  $credentials;
    }

    /**
     * Connection instance
     *
     * @param Credentials $credentials
     * @return object
     */
    public static function instance(Credentials $credentials):object {
        if (self::$instance == null)
        {
        self::$instance = new Connection ($credentials);
        }
 
        return self::$instance;
    }

    /**
     * Display a listing of the resource.
     * @param string $method
     * @param array $params
     * @return object
     */
    public function request (string $method, array $params) {
        // Pass jwt_token to request params
        if (!empty($this->credentials->jwt_token)) {
            $params['jwt_token'] = $this->credentials->jwt_token;
        }

        // Prepare body
        $body = [
            'jsonrpc' =>  '2.0',
            'method' => $method,
            'params' => $params,
        ];

        try {
            $guzzle = new Client([
                'verify' => false
            ]);
            $request = $guzzle->post('https://' . $this->credentials->url .  '/api', [
                'headers' => [ 
                    'Authorization' => 'Bearer ' . $this->credentials->token,
                    'Origin' => 'https://' . $this->credentials->url,
                    'Accept' => 'application/json'
                ],
                'json' => $body,
            ]);

            // Response
            $json_response = $request->getBody()->getContents();
            $response = json_decode($json_response);

            // Error handling
            if (isset($response->error)) {
                throw new RequestError($response->error->message, $response->error->data);
            }
            return $response->result;

        } catch (ClientException $e) {
            $error = Psr7\Message::toString($e->getResponse());
            throw $error;
        }
    }
}