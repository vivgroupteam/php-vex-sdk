<?php
namespace VivDesign\PhpVexSdk\Product;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Store pricelist parameters class
 * @var $code string
 * @var $type string
 * @var $base_pricegroup int
 * @var $priority int
 * @var $currency_id int
 * @var $status bool
 * @var $starts_at string
 * @var $ends_at string
 * @var $prices array
 * [
 *        'value' => 240,
 *        'product_id' => 1
 * ]
 */

class PricelistStore extends Params {
    /**
     * 
     * @var $code string
     */
    public string $code;

    /**
     * 
     * @var $type string
     */
    public string $type;

    /**
     * 
     * @var $priority int
     */
    public int $priority;

    /**
     * 
     * @var $currency_id int
     */
    public int $currency_id;

    /**
     * 
     * @var $base_pricegroup int
     */
    public int $base_pricegroup;

    /**
     * 
     * @var $status bool
     */
    public bool $status;

    /**
     * 
     * @var $starts_at string
     */
    public string $starts_at;

    /**
     * 
     * @var $ends_at string
     */
    public string $ends_at;

    /**
     * 
     * @var $prices array
     */
    public array $prices;


    /**
     * Product store pricelist constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'product.pricelist.store', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}