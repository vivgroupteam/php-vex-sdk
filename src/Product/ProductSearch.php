<?php
namespace VivDesign\PhpVexSdk\Product;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Search parameters class
 * @var $with array
 * @var $sorting array
 * @var $filters array
 * @var $groupNmclByCode bool
 */

class ProductSearch extends Params {
    /**
     * 
     * @var $with array
     */
    public array $with;

    /**
     * 
     * @var $sorting array
     */
    public array $sorting;

    /**
     * 
     * @var $filters array
     */
    public array $filters;

    /**
     * 
     * @var $groupNmclByCode bool
     */
    public bool $groupNmclByCode;

    /**
     * Product search constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'product.product.search', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}