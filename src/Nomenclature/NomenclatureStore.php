<?php
namespace VivDesign\PhpVexSdk\Nomenclature;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Nomenclatures store parameters class
 * @var $status bool
 * @var $order int
 * @var $extended_by string
 * @var $code string
 * @var $parent int
 * @var $parentCode string
 * @var $group array
 * @var $members array
 * @var $translations[Locale] array
 * @var $icon array
 * @var $user_id int
 */

class NomenclatureStore extends Params {
    /**
     * 
     * @var $status bool
     */
    public bool $status;

    /**
     * 
     * @var $code string
     */
    public string $code;

    /**
     * 
     * @var $extended_by string
     */
    public string $extended_by;

    /**
     * 
     * @var $order int
     */
    public int $order;

    /**
     * 
     * @var $parent int
     */
    public int $parent;

    /**
     * 
     * @var $parentCode string
     */
    public string $parentCode;

    /**
     * 
     * @var $group array
     */
    public array $group;

    /**
     * 
     * @var $members array
     */
    public array $members;

    /**
     * @var array $translations[Locale]
     */
    public array $translations = [
        // 'en' => [
        //     'name' => '',
        //     'description' => ''
        // ]
    ];

    /**
     * @var array $icon
     */
    public array $icon = [
        //'filename' => '',
        //'content' => '"data:image/jpeg;base64,...',
    ];

    /**
     * 
     * @var $user_id int
     */
    public int $user_id;


    /**
     * Nomenclature store constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'nomenclature.nomenclature.store', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}