<?php
namespace VivDesign\PhpVexSdk\Company;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Nomenclatures store parameters class
 * @var $status bool
**/

class CompanyStore extends Params {
    /**
     * 
     * @var $status bool
     * @var $type string person|company
     * @var $company_id string
     * @var $vat string
     * @var $country string
     * @var $city string
     * @var $address string
     * @var $custodian_name string
     * @var $contact_name string
     * @var $contact_phone string
     */
    public bool $status;

    /**
     * 
     * @var $type string person|company
     */
    public string $type;

    /**
     * 
     * @var $company_id string
     */
    public string $company_id;

    /**
     * 
     * @var $vat string
     */
    public string $vat;

    /**
     * 
     * @var $country string
     */
    public string $country;

    /**
     * 
     * @var $city string
     */
    public string $city;

    /**
     * 
     * @var $address string
     */
    public string $address;

    /**
     * 
     * @var $custodian_name string
     */
    public string $custodian_name;

    /**
     * 
     * @var $contact_name string
     */
    public string $contact_name;

    /**
     * 
     * @var $contact_phone string
     */
    public string $contact_phone;

    /**
     * 
     * @var $parentCode string
     */
    public string $parentCode = 'company';

     /** 
     * @var $nmclType string
     */
    public string $nmclType = 'company.company';

    /**
     * @var array $translations[Locale]
     */
    public array $translations = [
        // 'en' => [
        //     'name' => ''
        // ]
    ];


    /**
     * Compay store constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        $params['parentCode'] = $this->parentCode;
        $params['nmclType'] = $this->nmclType;

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'company.company.store', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}