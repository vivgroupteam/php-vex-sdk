<?php
namespace VivDesign\PhpVexSdk\Product;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * List parameters class
 * @var $with array
 * @var $mutations array
 * @var $collection array
 * @var $paginate bool
 * @var $groupNmclByCode bool
 * @var $groupPricesByCode bool
 * @var $withNmclPairsGroup string
 */

class ProductList extends Params {
    /**
     * 
     * @var $with array
     */
    public array $with;

    /**
     * 
     * @var $mutations array
     */
    public array $mutations;

    /**
     * 
     * @var $collection array
     */
    public array $collection;

    /**
     * 
     * @var $paginate bool
     */
    public bool $paginate;

    /**
     * 
     * @var $groupNmclByCode bool
     */
    public bool $groupNmclByCode;

    /**
     * 
     * @var $groupPricesByCode bool
     */
    public bool $groupPricesByCode;

    /**
     * 
     * @var $withNmclPairsGroup string
     */
    public string $withNmclPairsGroup;

    /**
     * Product list constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'product.product.list', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}