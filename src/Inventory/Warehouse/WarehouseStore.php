<?php
namespace VivDesign\PhpVexSdk\Inventory\Warehouse;

use VivDesign\PhpVexSdk\Connection;
use VivDesign\PhpVexSdk\Credentials;
use VivDesign\PhpVexSdk\Params;

/**
 * Store parameters class
 * @var $code string
 * @var $status bool
 * @var $order int
 * @var $translations array
 * [
 *    'en' => [
 *        'name' => 'Store name',
 *        'description' => 'Store description'
 *    ] 
 * ]
 */

class WarehouseStore extends Params {
    /**
     * 
     * @var $code string
     */
    public string $code;

    /**
     * 
     * @var $status bool
     */
    public bool $status;

    /**
     * 
     * @var $order int
     */
    public int $order;

    /**
     * 
     * @var $translations array
     */
    public array $translations;

    /**
     * Warehouse store constructor
     *
     * @param Credentials $credentials
     * @param array $params
     * @param array $schema
     */
    public function __construct (
        Credentials $credentials, 
        array $params, 
        array $schema = []
    ) {
        // Instance the connection class
        $connection = Connection::instance($credentials);

        // If schema is provided prepare params
        if (!empty ($schema)) {
            $params = $this->prepareParamsBySchema($params, $schema);
        }

        // Validate params types
        $this->validateParams($params);

        // Set params
        $this->setParams($params);

        // Make request
        $response = $connection->request(
            'inventory.warehouse.store', 
            self::toArray()
        );

        // Set response
        $this->setResponse($response);
    }  
}